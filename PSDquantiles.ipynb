{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Quantile statistics for PSD"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Intention of this notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Welch's ([1967](https://doi.org/10.1109/TAU.1967.1161901)) algorithm for the computation of power spectral density (PSD) for time series data splits the time series into many segments, computes the FFT for each segment, scaled to units of PSD and takes the average over all segments.\n",
    "This is equivalent to taking the FFT for the entire time series and then taking the average over adjacent Fourier coefficients.\n",
    "This notebook demonstrates that a percentile statistic which extracts the median values for the coefficients in all segments cannot replace the averaging.\n",
    "Resulting values of PSD would systematically be too small.\n",
    "Quantile statistics may only be applied to PSD spectra, which are obtained by proper averaging prior to analyzing quantiles (this is demonstrated at the very end of this notebook)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Introductory remarks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PSD analysis in seismology sometimes makes use of quantile statistics in order to establish low-noise levels.\n",
    "The idea behind that is, that transient signals of large amplitude (earthquakes) would not affect the smaller percentiles.\n",
    "One might be tempted to replace the averaging in PSD computation by taking the median in the quantile statistics for scaled FFT coeffients.\n",
    "This however results in too small values when compared with the actual PSD.\n",
    "This notebook demonstrates this effect by analyzing white noise, for which the actual value of PSD is known by Parceval's theorem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PSD is defined based on the normalized autocorrelation of an infinite stationary and continuous signal with stochastic phase.\n",
    "In time series analysis, we estimate PSD based on a sampled representation of the signal of finite duration.\n",
    "The PSD then is computed based on an appropriately scaled FFT of the time series ([Forbriger, 2022, section 5](http://dx.doi.org/10.5445/IR/1000158425)).\n",
    "By selecting a finite sequence from the random phase time series, some of the Fourier coefficients to not carry signal energy, because of the random signal phase letting the Fourier component cancel in the respective time window. \n",
    "The signal energy then is carried by the neighboring Fourier coefficients.\n",
    "To obtain a meaningful value of PSD for a finite time series, values of adjacent PSD coefficients must be averaged, necessarily.\n",
    "Welch's ([1967](https://doi.org/10.1109/TAU.1967.1161901)) algorithm replaces this averaging by taking the average over PSD-scaled FFT coefficients from a set of subsegments of the time series.\n",
    "Both approaches are equivalent."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This averaging may not be replaced by taking the median for the PSD-scaled FFT coefficients, because this will put emphasis on the coefficients, which cancel due to the stochastic phase nature of the signal, resulting in systematically too small PSD values.\n",
    "If quantile statistics shall be applied (in order to reduce the effect of non-stationarity of the signal, emphasizing windows of small PSD), the statistics have to be applied to PSD spectra, which are obtained by proper averaging FFT coefficients."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Three series of white noise are produced with the program [randomseries](https://git.scc.kit.edu/Seitosh/Seitosh/-/blob/master/src/synt/misc/randomseries.cc) and a high-quality random number generator from [libgsl](https://www.gnu.org/software/gsl/doc/html/rng.html) are available.\n",
    "The series consist of gaussian distributed uncorrelated random numbers with a given variance and mean value.\n",
    "The time series are provided in [Seitosh ascii format](https://git.scc.kit.edu/Seitosh/Seitosh/-/tree/master/src/libs/libdatrwxx/ascii) in files\n",
    "- `white1.asc`\n",
    "- `white2.asc`\n",
    "- `white3.asc`\n",
    "\n",
    "The current notebook only uses one time series (see below)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prepare for later computation\n",
    "### Import python modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.signal as signal\n",
    "from obspy.signal import PPSD\n",
    "from scipy.signal import spectrogram\n",
    "from obspy import Stream\n",
    "import matplotlib.mlab as mlab"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from readcroposp import readcroposp\n",
    "from seitoshio import readascii"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Do some elementary settings for diagram displays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams[\"figure.figsize\"] = (20,10)\n",
    "plt.rcParams[\"axes.titlesize\"] = 24\n",
    "plt.rcParams[\"axes.labelsize\"] = 20\n",
    "plt.rcParams[\"font.size\"] = 20\n",
    "plt.rcParams[\"xtick.labelsize\"] = 15\n",
    "plt.rcParams[\"ytick.labelsize\"] = 15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyze time series data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read time series data.\n",
    "The time series provided in files `white1.asc`, `white2.asc`, and `white2.asc` are composed of uncorrelated gaussian distributed random numbers with a constant offset.\n",
    "They are time series of stationary white random noise, because the random numbers are uncorrelated.\n",
    "The parameters in terms of signal power, number of samples, sampling interval, and offset are identical for all three of them.\n",
    "They are just three different representations of random sequences with these parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read the time series data. \n",
    "Because of the large number of samples, this may take a while.\n",
    "Let the functions output a lot of information, just for the impatient."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the current test, we just load one of the random sequences and we remove the offset right away."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seqno=1\n",
    "filename='white%d.asc' % seqno\n",
    "inputseries=readascii(filename, verbose=True, demean=True)\n",
    "label=\"%s.%s.%s\" % (inputseries.stats.station, \n",
    "                    inputseries.stats.instype,\n",
    "                    inputseries.stats.channel)\n",
    "inputseries.stats.update({\"filename\": filename,\n",
    "                          \"label\": label})\n",
    "# sampling interval\n",
    "dt=inputseries.stats.delta\n",
    "# sampling rate\n",
    "samplingrate=1./dt\n",
    "# Nyquist frequency\n",
    "fNy=0.5*samplingrate\n",
    "# time series\n",
    "whitedata=inputseries.data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute PSD level by using Parceval's theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Parceval's theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If $a(t)$ is the time domain signal and\n",
    "$$\n",
    "P(\\tau)=\\lim_{T\\rightarrow\\infty}\\frac{1}{2T}\\int\\limits_{-T}^{+T}a(t)\\,a(t+\\tau)\\,\\text{d}t\n",
    "$$\n",
    "is the normalized autocorrelation, then\n",
    "$$\n",
    "\\tilde{P}(\\omega)=\\int\\limits_{-\\infty}^{+\\infty}P(\\tau)\\,e^{-i\\omega \\tau}\\,\\text{d}\\tau\n",
    "$$\n",
    "is the *two-sided* power spectral density (PSD)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Average total signal power is\n",
    "$$\n",
    "\\overline{P}=\\lim_{T\\rightarrow\\infty}\\frac{1}{2T}\\int\\limits_{-T}^{+T}a(t)^2\\,\\text{d}t=P(\\tau=0).\n",
    "$$\n",
    "If we understand $\\tilde{P}(\\omega)$ as part of a Fourier transformation pair, then\n",
    "$$\n",
    "P(\\tau)=\\int\\limits_{-\\infty}^{+\\infty}\\tilde{P}(\\omega)\\,e^{i\\omega \\tau}\\,\\frac{\\text{d}\\omega}{2\\pi}.\n",
    "$$\n",
    "This way we can express the average total signal power by\n",
    "$$\n",
    "\\overline{P}=P(\\tau=0)=\\int\\limits_{-\\infty}^{+\\infty}\\tilde{P}(\\omega)\\,\\frac{\\text{d}\\omega}{2\\pi}.\n",
    "$$\n",
    "Hence\n",
    "$$\n",
    "\\int\\limits_{-\\infty}^{+\\infty}\\tilde{P}(\\omega)\\,\\frac{\\text{d}\\omega}{2\\pi}=\\lim_{T\\rightarrow\\infty}\\frac{1}{2T}\\int\\limits_{-T}^{+T}a(t)^2\\,\\text{d}t,\n",
    "$$\n",
    "which essentially is *Parceval's theorem*.\n",
    "From this it is obvious that stationary signals must be band-limited in order to have finite average total signal power.\n",
    "A not band-limited signal would have infinite power.\n",
    "\n",
    "If $P_\\text{PSD}$ is a frequency independent level of *one-sided* power spectral density (PSD), as is the case for white noise, such that\n",
    "$$\n",
    "\\tilde{P}(\\omega)=\\begin{cases}\\frac{P_\\text{PSD}}{2}\\text{ if $|\\omega|<2\\pi f_{\\text{Ny}}$ and}\\\\ 0\\text{ otherwise,}\\end{cases}\n",
    "$$\n",
    "where $f_{\\text{Ny}}$ would be the Nyquist-frequency for a discrete time series, then the integration can be replaced by\n",
    "$$\n",
    "\\tilde{P}(\\omega)\\,2 f_{\\text{Ny}}=P_\\text{PSD}\\,f_{\\text{Ny}}=a^2_{\\text{rms}},\n",
    "$$\n",
    "where \n",
    "$$\n",
    "a_{\\text{rms}}=\\sqrt{\\frac{1}{2T}\\int\\limits_{-T}^{+T}a(t)^2\\,\\text{d}t}\n",
    "$$\n",
    "is the rms-amplitude of the stationary time domain signal.\n",
    "In consequence\n",
    "$$\n",
    "P_\\text{PSD}=\\frac{a^2_{\\text{rms}}}{f_{\\text{Ny}}}.\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Numerical computation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The rms value of the time series with $N$ samples $x_l$ is\n",
    "$$\n",
    "a_{\\text{rms}}=\\sqrt{\\frac{1}{N}\\,\\sum\\limits_{l=1}^{N}\\,(x_l-\\overline{x})^2}\n",
    "$$\n",
    "with\n",
    "$$\n",
    "\\overline{x}=\\sum\\limits_{l=1}^{N}\\,x_l.\n",
    "$$\n",
    "The total average power is $P=a^2_{\\text{rms}}$, where for white noise the power is uniformly distributed over the frequency band from 0 Hz to $f_{\\text{Ny}}$.\n",
    "The one-sided PSD hence is\n",
    "$$\n",
    "P_{\\text{PSD}}=\\frac{a^2_{\\text{rms}}}{f_{\\text{Ny}}}.\n",
    "$$\n",
    "\n",
    "Compute $P_{\\text{PSD}}$ for the time series and store the value in `whitePSD`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nsamples=len(whitedata)\n",
    "average=np.mean(whitedata)\n",
    "print(\"  sampling interval %6.3fs, %d samples\" % (dt, nsamples))\n",
    "print(\"  average value: %12.4f counts\" % (average))\n",
    "rms=np.sqrt(np.mean(np.power((whitedata-average),2)))\n",
    "print(\"  rms amplitude: %12.4f counts\" % rms)\n",
    "power=rms*rms\n",
    "print(\"  signal power:  %12.4f counts^2\" % power)\n",
    "whitePSD=power/fNy\n",
    "print(\"  one-sided PSD: %12.4f counts^2 Hz^(-1)\" % whitePSD)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Compute PSD by scipy-implementation of Welch's algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# number of samples per segment\n",
    "nperseg=500\n",
    "# number of overlapping samples between segments\n",
    "noverlap=int(0.5*nperseg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run computation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, psd=signal.welch(whitedata, 1./dt, nperseg=nperseg, noverlap=noverlap)\n",
    "# discard value at 0 Hz and value at Nyquist frequency\n",
    "welchPSD=[f[1:-1], psd[1:-1]]    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot PSD"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minf=np.min(f)\n",
    "maxf=fNy\n",
    "plt.figure()\n",
    "plt.title('Comparison of one-sided PSD')\n",
    "plt.xlabel('frequency / Hz')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "[f, psd]=welchPSD\n",
    "plt.loglog(f, psd, ls=':',linewidth=3, label=\"scipy.signal.welch\")\n",
    "plt.loglog([minf, maxf], [whitePSD, whitePSD], \n",
    "           label=\"time domain power / bandwidth\", linewidth=5, ls=':')\n",
    "plt.grid(linewidth=0.5, linestyle='--')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute spectral coefficients for segments of the time series\n",
    "\n",
    "Make use of `scipy.signal.sepctrogram` to split the time series into segments, taper each segment and compute PSD coefficients from appropriatlyscaled FFT coefficients.\n",
    "This is what Welch's algorithm does.\n",
    "Welch's algorithm in addition takes the average over the PSD spectra for all segments.\n",
    "`scipy.signal.sepctrogram` returns the individual PSD spectra."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute expected dimensions\n",
    "print(\"expected number of frequency values: %d\" % (int(nperseg/2+1)))\n",
    "# number of signal windows\n",
    "print(\"expected number of signal windows: %d\" % (int(len(whitedata)/(nperseg-noverlap))-1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ffc, tt, Sc = spectrogram(whitedata, \n",
    "                        fs=1./dt, \n",
    "                        window='hann', \n",
    "                        scaling='density',\n",
    "                        return_onesided=True,\n",
    "                        mode='psd',\n",
    "                        nperseg=nperseg, \n",
    "                        noverlap=noverlap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the dimentsions of `Sc` match the expected dimensions, this indicates that `scipy.signal.sepctrogram` returns one spectrum per signal segment and that no averaging takes place."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(nf, nw)=Sc.shape\n",
    "print(\"number of frequency values: %d\" % nf)\n",
    "print(\"number of signal windows: %d\" % nw)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# discard values at zero and Nyquist frequency\n",
    "ff=ffc[1:-1]\n",
    "S=Sc[1:-1,:]\n",
    "print(S.shape)\n",
    "(nf, nw)=S.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Display the PSD values for the segments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minf=np.min(f)\n",
    "maxf=fNy\n",
    "ispecmin=0\n",
    "ispecmax=nw\n",
    "plt.figure()\n",
    "plt.title('One-sided PSD from scipy.signal.spectrogram')\n",
    "plt.xlabel('frequency / Hz')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "for i in range(ispecmin, ispecmax):\n",
    "    plt.loglog(ff, S[:,i], ls=':',linewidth=0.5)\n",
    "plt.loglog([minf, maxf], [whitePSD, whitePSD], \n",
    "           label=\"time domain power / bandwidth\", \n",
    "           linewidth=1.0, ls='-', color='black')\n",
    "plt.grid(linewidth=0.3, linestyle='--', color='grey')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The PSD spectra for the segments contain the total average signal power, each.\n",
    "The way this power is distributed over the FFT coefficients highly differs for each window.\n",
    "By selecting a finite sequence from the random phase time series, some of the Fourier coefficients to not carry signal energy, because of the random signal phase letting the Fourier component cancel in the respective time window. \n",
    "The signal energy then is carried by the neighboring Fourier coefficients.\n",
    "The individual coefficients, although the PSD at each frequency is the same for the white noise signal in theory, substantially scatter about the true PSD value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compute total power and PSD in each segment\n",
    "\n",
    "In this block we average values of coefficients returnd by `scipy.signal.spectrogram` to demonstrate how averaging lets us approach the true PSD value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let \n",
    "$$\n",
    "a_l=a(t_l)=a(l\\,\\Delta t)\n",
    "$$\n",
    "be the time series representing the signal $a(t)$ \n",
    "being sampled with an interval of $\\Delta t$.\n",
    "Then\n",
    "$$\n",
    "\\tilde{a}_k=\\sum\\limits_{l=1}^{N} \\,a_l\\,e^{-2i\\pi\\frac{(l-1)\\,(k-1)}{N}}\\Delta t\n",
    "$$\n",
    "is the Fourier coeffient for $a_l$ at frequency $f_k$ in the sense\n",
    "of the Fourier transform $\\tilde{a}(2\\pi f_k)=\\tilde{a}_k$ being sampled."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parceval's theorem means for the energy\n",
    "$$\n",
    "E=\\int\\limits_{-\\infty}^{\\infty}|a(t)|^2\\,\\text{d}t\n",
    "=\\int\\limits_{-\\infty}^{\\infty}|\\tilde{a}(\\omega)|^2\\,\\frac{\\text{d}\\omega}{2\\pi},\n",
    "$$\n",
    "of a transient signal,\n",
    "which is equivalent to\n",
    "$$\n",
    "E=\\sum\\limits_{l=1}^{N} |a_l|^2 \\Delta t= \n",
    "2\\sum\\limits_{k=1}^{N/2} |\\tilde{a}_k|^2 \\frac{1}{N\\Delta t}.\n",
    "$$\n",
    "for the series, where $$\\Delta \\omega=\\frac{2\\pi}{N\\Delta t}$$\n",
    "and summation is carried out only at positive frequency \n",
    "(hence the factor of 2 in front of the sum on the right-hand-side)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The total average power of the signal is\n",
    "$$\n",
    "\\overline{P}=\\frac{E}{N\\Delta t}=\\frac{1}{N}\\sum\\limits_{l=1}^{N} |a_l|^2 \n",
    "=\\frac{2}{(N\\,\\Delta t)^2}\\sum\\limits_{k=1}^{N/2} |\\tilde{a}_k|^2.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The FFT scaled to units of one-sided power spectral density, \n",
    "like computed by `scipy.signal.spectrogram`\n",
    "is\n",
    "$$\n",
    "2\\tilde{P}(2\\pi f_k)=2\\frac{|\\tilde{a}_k|^2}{N\\,\\Delta t}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Total average power then is\n",
    "$$\n",
    "\\overline{P}=2\\sum\\limits_{k=1}^{N/2}\\,\\tilde{P}(2\\pi f_k)\\,\\frac{\\Delta\\omega}{2\\pi}\n",
    "=\\frac{2}{N\\Delta t}\\sum\\limits_{k=1}^{N/2}\\,\\tilde{P}(2\\pi f_k)\n",
    "=\\frac{2}{(N\\Delta t)^2}\\sum\\limits_{k=1}^{N/2}\\,|\\tilde{a}_k|^2\n",
    "$$\n",
    "thus\n",
    "$$\n",
    "\\overline{P}=\\frac{1}{2\\Delta t}\\frac{2}{N}\\sum\\limits_{k=1}^{N/2}\\,2\\tilde{P}(2\\pi f_k).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compute the total average signal power\n",
    "\n",
    "Compute the total average power per segment and store thsi in `P`.\n",
    "Compute PSD in each segment and store this in `SPSD`.\n",
    "Compute PSD of the entire signal by averaging `SPSD`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute total average power in each window\n",
    "P=np.mean(S,axis=0)/(2.*dt)\n",
    "# compute power spectral density of white noise for each window\n",
    "SPSD=P/fNy\n",
    "print(\"average power spectral density: %12.4f counts^2 Hz^(-1)\" % np.mean(SPSD))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Check whether white noise PSD in each window matches PSD from time domain\n",
    "\n",
    "Investigate the distribution of total average power for individual windows.\n",
    "First plot $\\overline{P}$ for each window on a time scale."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.title('One-sided PSD for white noise in each window from scipy.signal.spectrogram')\n",
    "plt.xlabel('time / s')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "plt.semilogy(tt,SPSD)\n",
    "plt.semilogy([tt[0], tt[-1]], [whitePSD, whitePSD], \n",
    "           label=\"time domain power / bandwidth\", \n",
    "           linewidth=1.0, ls='-', color='black')\n",
    "plt.grid(linewidth=0.3, linestyle='--', color='grey')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Check whether average PSD matches median from all windows\n",
    "\n",
    "Compute a percentile statistics for the total average signal power per segment.\n",
    "Notice that these values are obtained by proper averaging over the FFT computed values along the frequency axis.\n",
    "The resulting distribution is rather narrow and the median matches the averag value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.title('One-sided PSD for white noise in each window from scipy.signal.spectrogram')\n",
    "plt.xlabel('percentile')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "percent=np.arange(0,100,100./len(SPSD))\n",
    "plt.semilogy(percent,np.sort(SPSD))\n",
    "plt.semilogy([percent[0], percent[-1]], [whitePSD, whitePSD], \n",
    "           label=\"time domain power / bandwidth\", \n",
    "           linewidth=1.0, ls='-', color='black')\n",
    "avgSPSD=np.mean(SPSD)\n",
    "plt.semilogy([percent[0], percent[-1]], [avgSPSD, avgSPSD], \n",
    "           label=\"average PSD\", \n",
    "           linewidth=2.0, ls='--', color='red')\n",
    "plt.grid(linewidth=0.3, linestyle='--', color='grey')\n",
    "plt.xticks(np.arange(0.,110.,10.))\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compute percentile spectra from windows"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each single FFT spectrum contains full power.\n",
    "A quantile spectrum extracts the respective quantile from all FFT spectra and no longer contains the full signal power.\n",
    "We compare percentile spectra and the median spectrum in particular with the true values of PSD and the average PSD spectrum, obtained by averaging the FFT derived spectra from all segments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "percentiles=[5,10, 50, 90,95]\n",
    "percentilePSD=np.percentile(S, percentiles, axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take the average. This is what Welch's algorithm actually does."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "meanPSD=np.mean(S, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minf=np.min(f)\n",
    "maxf=fNy\n",
    "plt.figure()\n",
    "plt.title('One-sided PSD from scipy.signal.spectrogram')\n",
    "plt.xlabel('frequency / Hz')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "for [p, pPSD] in zip(percentiles, percentilePSD):\n",
    "    plt.loglog(ff, pPSD, ls=':',linewidth=3.0,\n",
    "              label=('%2.0fth percentile' % p))\n",
    "plt.loglog(ff, meanPSD, ls='--',linewidth=3.0,\n",
    "           color='red', label=('mean'))\n",
    "plt.loglog([minf, maxf], [whitePSD, whitePSD], \n",
    "           label=\"time domain power / bandwidth\", \n",
    "           linewidth=1.0, ls='-', color='black')\n",
    "plt.grid(linewidth=0.3, linestyle='--', color='grey')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The median (50th percentile) systematically is lower than the actual PSD."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compute percentile statistics after averaging\n",
    "\n",
    "Averaging several FFT derived PSD spectra before the application of the quantile statistics heals the problem.\n",
    "This is what actually should be done when deriving low-noise models. \n",
    "For a set of time windows proper PSD spectra are computed by Welch's algorithm.\n",
    "The application of quantile statistics then helps to reduce the effect of time windows, which contain earthquake signals.\n",
    "This is a proper alternative to taking an envelope of a set of PSD spectra."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# number of windows to average\n",
    "Navg=100\n",
    "# number of groups of windows\n",
    "Ng=int(nw/Navg)\n",
    "# number of windows needed for reshaping\n",
    "Nww=Navg*Ng\n",
    "# select submatrix appropriate for reshaping\n",
    "Ssub=S[:,:Nww]\n",
    "# reshape array\n",
    "Srs=np.reshape(Ssub,(nf,Navg,Ng))\n",
    "# compute average\n",
    "Savg=np.mean(Srs,axis=1)\n",
    "print(Savg.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minf=np.min(f)\n",
    "maxf=fNy\n",
    "ispecmin=0\n",
    "ispecmax=Ng\n",
    "plt.figure()\n",
    "plt.title('One-sided PSD from scipy.signal.spectrogram after proper averaging')\n",
    "plt.xlabel('frequency / Hz')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "for i in range(ispecmin, ispecmax):\n",
    "    plt.loglog(ff, Savg[:,i], ls=':',linewidth=0.5)\n",
    "plt.loglog([minf, maxf], [whitePSD, whitePSD], \n",
    "           label=\"time domain power / bandwidth\", \n",
    "           linewidth=1.0, ls='-', color='black')\n",
    "plt.grid(linewidth=0.3, linestyle='--', color='grey')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compute percentiles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "percentiles=[5,10, 50, 90,95]\n",
    "percentilePSD=np.percentile(Savg, percentiles, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minf=np.min(f)\n",
    "maxf=fNy\n",
    "plt.figure()\n",
    "plt.title('One-sided PSD from scipy.signal.spectrogram after proper averaging')\n",
    "plt.xlabel('frequency / Hz')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "for [p, pPSD] in zip(percentiles, percentilePSD):\n",
    "    plt.loglog(ff, pPSD, ls=':',linewidth=3.0,\n",
    "              label=('%2.0fth percentile' % p))\n",
    "plt.loglog(ff, meanPSD, ls='--',linewidth=3.0,\n",
    "           color='red', label=('mean'))\n",
    "plt.loglog([minf, maxf], [whitePSD, whitePSD], \n",
    "           label=\"time domain power / bandwidth\", \n",
    "           linewidth=1.0, ls='-', color='black')\n",
    "plt.grid(linewidth=0.3, linestyle='--', color='grey')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Due to proper averaging when computing the individual PSD spectra, the median now is at the level of the average PSD spectrum and the true PSD."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
