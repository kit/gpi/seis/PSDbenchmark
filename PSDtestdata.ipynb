{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Check consistency of PSD computation using recorded data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Intention of this notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We check the validity of power spectral density (PSD) levels computed by different implementations.\n",
    "The test data are recordings from BFO.\n",
    "These data shall be analyzed by different approaches to demonstrate that they all provide the same values of PSD.\n",
    "\n",
    "PSD values are computed in three ways:\n",
    "1. by the program [foutra](https://git.scc.kit.edu/Seitosh/Seitosh/-/blob/master/src/ts/wf/foutra.cc)\n",
    "2. by `scipy.signal.welch`\n",
    "3. PSD data extracted from an instance of `obspy.signal.PPSD`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Computations not carried out in python are controlled by the accompanying `Makefile`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time series data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time series data is provided in `recording.asc` for the vertical component of the STS-2 at BFO.\n",
    "Samples are given in units of $\\mu\\text{m}\\,\\text{s}^{-1}$ in the passband of the seismometer, i.e. for frequency larger than $0.01\\,\\text{Hz}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### PSD values computed with `foutra`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameters used for the computation with `foutra` are set in the `Makefile`.\n",
    "The results computed for the three time series are provided in an ASCII table in files `recording_std.tab` and `recording_nosmooth.tab`.\n",
    "The first uses averaging along the frequency axis for smoothing, the latter does not.\n",
    "The first column specifies frequency for each line in units of Hertz.\n",
    "The second column specifies PSD in units of $\\mu\\text{m}^2\\,\\text{s}^{-1}\\, \\text{Hz}^{-1}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prepare for later computation\n",
    "### Import python modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.signal as signal\n",
    "from obspy.signal import PPSD\n",
    "from scipy.signal import spectrogram\n",
    "from obspy import Stream"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from obspy.signal.spectral_estimation import get_nlnm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from readcroposp import readcroposp\n",
    "from seitoshio import readascii"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Do some elementary settings for diagram displays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams[\"figure.figsize\"] = (20,10)\n",
    "plt.rcParams[\"axes.titlesize\"] = 24\n",
    "plt.rcParams[\"axes.labelsize\"] = 20\n",
    "plt.rcParams[\"font.size\"] = 20\n",
    "plt.rcParams[\"xtick.labelsize\"] = 15\n",
    "plt.rcParams[\"ytick.labelsize\"] = 15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read time series data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read the time series data. \n",
    "Because of the large number of samples, this may take a while.\n",
    "Let the functions output a lot of information, just for the impatient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "recording=readascii('recording.asc', verbose=True, demean=False)\n",
    "recording.stats.network=recording.stats[\"auxid\"]\n",
    "recording.stats.update({\"filename\": 'recording.asc',\n",
    "                          \"label\": recording.id})\n",
    "print('\\n', recording.stats)\n",
    "# store Nyquist frequency as the upper limit for the frequency scale\n",
    "fNy=0.5/recording.stats.delta"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read PSD values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read `foutra` results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foutra_stdPSD=readcroposp('recording_std.tab', verbose=True)\n",
    "foutra_nosmoothPSD=readcroposp('recording_nosmooth.tab', verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compute PSD"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### use `scipy.signal.welch`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nperseg=1000\n",
    "noverlap=0.5*nperseg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run computation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"analyse %s\" % recording)\n",
    "dt=recording.stats.delta\n",
    "f, psd=signal.welch(recording.data, 1./dt, nperseg=nperseg, noverlap=noverlap)\n",
    "# discard value at 0 Hz and value at Nyquist frequency\n",
    "welchPSD=[f[1:-1], psd[1:-1]]    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### use `scipy.spectrogram`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculate spectrogram"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Switch averaging on and of, to investigate the effect on the median value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "applyaveraging=False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ff, tt, S = spectrogram(recording.data, fs=1./dt, window='hann', \n",
    "                        nperseg=nperseg, noverlap=noverlap)\n",
    "(nf, nw)=S.shape\n",
    "print(\"number of frequency values: %d\" % nf)\n",
    "print(\"number of signal windows: %d\" % nw)\n",
    "if applyaveraging:\n",
    "    (nf, nw)=S.shape\n",
    "    # number of windows to average\n",
    "    Navg=40\n",
    "    # number of groups of windows\n",
    "    Ng=int(nw/Navg)\n",
    "    # number of windows needed for reshaping\n",
    "    Nww=Navg*Ng\n",
    "    # select submatrix appropriate for reshaping\n",
    "    Ssub=S[:,:Nww]\n",
    "    # reshape array\n",
    "    Srs=np.reshape(Ssub,(nf,Navg,Ng))\n",
    "    # compute average\n",
    "    Savg=np.mean(Srs,axis=1)\n",
    "    print(Savg.shape)\n",
    "    S=Savg.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build PPSD"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a, b      = np.shape(S)\n",
    "S_tot     = np.empty((a, 0), dtype = 'float64')\n",
    "S_tot     = np.append(S_tot, S, axis=1)\n",
    "bb        = np.logspace(-10, 0, 200)\n",
    "\n",
    "Shist= np.empty((len(bb)-1, len(ff)))\n",
    "for i in range(len(ff)):\n",
    "        Shist[:,i] = np.histogram(S_tot[i,:], bins=bb)[0]/len(tt)\n",
    "\n",
    "bb        = np.delete(bb, -1)\n",
    "\n",
    "Shist[Shist == 0] = np.nan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot PPSD"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nlnmper, nlnmaccdbpsd=get_nlnm()\n",
    "nlnmf=1./nlnmper\n",
    "nlnmom=nlnmf*2*np.pi\n",
    "nlnmaccpsd=np.power(10.,nlnmaccdbpsd/10.)\n",
    "nlnmvelpsd=nlnmaccpsd/(nlnmom*nlnmom)*1.e12\n",
    "\n",
    "plt.figure(figsize=(9,6))\n",
    "cmap = plt.get_cmap('RdYlBu_r')\n",
    "plt.pcolormesh(ff, bb, Shist, cmap=cmap)\n",
    "plt.colorbar().set_label(label='Probability',size=14)\n",
    "plt.xlabel('Frequency / Hz', fontsize=14)\n",
    "plt.ylabel(r\"PSD / m$^{2}$ s$^{-4}$ Hz$^{-1}$\", fontsize=14)\n",
    "plt.xscale('log')\n",
    "plt.yscale('log')\n",
    "plt.plot(nlnmf, nlnmvelpsd, label='NLNM', linewidth=5, color='black')\n",
    "plt.xlim([1e-2, 20])\n",
    "plt.ylim([1e-8, 1])\n",
    "plt.clim(0, np.nanmax(Shist)/np.sqrt(np.e))\n",
    "plt.grid()\n",
    "plt.legend(fontsize=10)\n",
    "plt.tick_params(labelsize=10)\n",
    "plt.tight_layout()\n",
    "plt.show()\n",
    "plt.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get Percentiles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = len(ff)\n",
    "        \n",
    "percentile_10 = np.zeros(a)\n",
    "percentile_50 = np.zeros(a)\n",
    "percentile_90 = np.zeros(a)\n",
    "    \n",
    "for i in range(a):\n",
    "        \n",
    "        somma  = np.nancumsum(Shist[:, i])\n",
    "        vp_10  = np.amax(somma)*0.1\n",
    "        vp_90  = np.amax(somma)*0.9\n",
    "        vp_50  = np.amax(somma)*0.5\n",
    "        \n",
    "        p_10   = np.where(somma <= vp_10)\n",
    "        p_90   = np.where(somma <= vp_90)\n",
    "        p_50   = np.where(somma <= vp_50)\n",
    "        \n",
    "        #print(vp_10, len(p_10[0]), len(p_50[0]), len(p_90[0]))\n",
    "        \n",
    "        if len(p_10[0]) != 0:\n",
    "            \n",
    "            percentile_10[i] = bb[p_10[0][len(p_10[0]) - 1]]\n",
    "            \n",
    "        else:\n",
    "            \n",
    "            percentile_10[i] = 0\n",
    "            \n",
    "        if len(p_50[0]) != 0:\n",
    "            \n",
    "            percentile_50[i] = bb[p_50[0][len(p_50[0]) - 1]]\n",
    "            \n",
    "        else:\n",
    "            \n",
    "            percentile_50[i] = 0\n",
    "        \n",
    "        if len(p_90[0]) != 0:\n",
    "            \n",
    "            percentile_90[i] = bb[p_90[0][len(p_90[0]) - 1]]\n",
    "            \n",
    "        else:\n",
    "                \n",
    "                percentile_90[i] = 0\n",
    "\n",
    "nppercentile_10, nppercentile_50, nppercentile_90=np.percentile(S, [10, 50, 90], axis=1)\n",
    "                \n",
    "plt.figure(figsize=(9,6))\n",
    "plt.plot(ff, (percentile_10), color = 'r', label = '10th Percentile')\n",
    "plt.plot(ff, (percentile_50), color = 'k', label = '50th Percentile')\n",
    "plt.plot(ff, (percentile_90), color = 'b', label = '90th Percentile')\n",
    "plt.plot(ff, (nppercentile_10), color = 'r', linestyle=':', label = '10th Percentile (numpy)')\n",
    "plt.plot(ff, (nppercentile_50), color = 'k', linestyle=':', label = '50th Percentile (numpy)')\n",
    "plt.plot(ff, (nppercentile_90), color = 'b', linestyle=':', label = '90th Percentile (numpy)')\n",
    "plt.plot(nlnmf, nlnmvelpsd, label='NLNM', linestyle = '--', linewidth = 3, color='black')\n",
    "plt.xlabel('Frequency / Hz', fontsize=14)\n",
    "plt.ylabel(r\"PSD / m$^{2}$ s$^{-4}$ Hz$^{-1}$\", fontsize=14)\n",
    "plt.ylim([1e-8, 1])\n",
    "plt.xlim([1e-2, 20])\n",
    "plt.legend(fontsize=10)\n",
    "plt.grid()\n",
    "plt.xscale('log')\n",
    "plt.yscale('log')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### use `obspy.signal.PPSD`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "paz = {'gain': 1., 'poles': [], 'sensitivity': 1., 'zeros': []}\n",
    "ppsd = PPSD(recording.stats, paz, ppsd_length=recording.stats.delta*nperseg*10)\n",
    "tr=recording.copy()\n",
    "# scale to m/s\n",
    "tr.data = tr.data * 1.e-6\n",
    "st=Stream(tr)\n",
    "ppsd.add(st)\n",
    "#print(ppsd.times_processed) \n",
    "ppsd.plot(xaxis_frequency=False) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ppsd_psd_list=ppsd.psd_values\n",
    "ppsd_period=ppsd.period_bin_centers\n",
    "ppsd_f=1./ppsd_period\n",
    "print(ppsd_f.shape)\n",
    "print(len(ppsd_psd_list))\n",
    "ppsd_psd=np.zeros(ppsd_f.shape)\n",
    "for p in ppsd_psd_list:\n",
    "# values are given in dB wrt to 1. m**2/(s**4 Hz)\n",
    "# convert to units of 1. um**2/(s**2 Hz)\n",
    "    p = 10.**(p/10.)*1.e12/(2.*np.pi*ppsd_f)**2\n",
    "    ppsd_psd = ppsd_psd + p\n",
    "ppsd_psd = ppsd_psd / len(ppsd_psd_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display results of PSD computation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute reference level (NLNM, Peterson, 1993)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get PSD values in dB w.r.t 1 m^2 s^-4 Hz^-1 \n",
    "nlnmper, nlnmaccdbpsd=get_nlnm()\n",
    "nlnmf=1./nlnmper\n",
    "nlnmom=nlnmf*2*np.pi\n",
    "# convert to PSD in units of 1 um^2 s^-2 Hz^-1\n",
    "nlnmaccpsd=np.power(10.,nlnmaccdbpsd/10.)\n",
    "nlnmvelpsd=nlnmaccpsd/(nlnmom*nlnmom)*1.e12\n",
    "# produce quick diagrams, just to check the conversion (no labels at axes)\n",
    "#plt.figure()\n",
    "#plt.semilogx(nlnmf,nlnmaccdbpsd)\n",
    "#plt.show()\n",
    "#plt.figure()\n",
    "#plt.loglog(nlnmf,nlnmaccpsd)\n",
    "#plt.show()\n",
    "#plt.figure()\n",
    "#plt.loglog(nlnmf,nlnmvelpsd)\n",
    "#plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create diagram"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minf=np.min(foutra_stdPSD[\"frequency\"])\n",
    "maxf=fNy\n",
    "maxpsd=100.\n",
    "minpsd=1.e-10\n",
    "plt.figure()\n",
    "plt.xlim([minf, maxf])\n",
    "plt.ylim([minpsd, maxpsd])\n",
    "plt.title('compare PSD values computed for\\n%s\\n' % str(recording))\n",
    "plt.xlabel('frequency / Hz')\n",
    "plt.ylabel(\"PSD / $\\mu$m$^{2}$ s$^{-2}$ Hz$^{-1}$\")\n",
    "plt.loglog(nlnmf, nlnmvelpsd, label='NLNM', linewidth=5, color='grey')\n",
    "for case in (foutra_stdPSD, foutra_nosmoothPSD):\n",
    "    for label in case:\n",
    "        if label != \"frequency\":\n",
    "            plt.loglog(case[\"frequency\"][1:], case[label][1:], ls='--', label=label)\n",
    "plt.loglog(welchPSD[0], welchPSD[1], ls=':', label=\"scipy.signal.welch\")\n",
    "plt.loglog(ppsd_f, ppsd_psd, label=\"obspy.signal.PPSD\")\n",
    "plt.loglog(ff, percentile_50, color = 'k', label = 'scipy.signal.spectrogram median')\n",
    "plt.loglog(ff, nppercentile_50, color = 'k', ls='--',\n",
    "           label = 'scipy.signal.spectrogram numpy median')\n",
    "plt.grid(linewidth=0.5, linestyle='--')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create diagram on dB scale"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### convert data in units of 1 um^2/(s^2 Hz) to 1 dB wrt 1 m^2/(s^4 Hz)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def convert_to_acc_dB(f, psd):\n",
    "    p=10*np.log10(psd * 1.e-12 * (2.* np.pi * f)**2)\n",
    "    return p"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minT=1./fNy\n",
    "maxT=np.max(1./foutra_stdPSD[\"frequency\"])\n",
    "maxpsd=-100.\n",
    "minpsd=-200.\n",
    "plt.figure()\n",
    "plt.xlim([minT, maxT])\n",
    "plt.ylim([minpsd, maxpsd])\n",
    "plt.title('compare PSD values computed for\\n%s\\n' % str(recording))\n",
    "plt.xlabel('period / s')\n",
    "plt.ylabel(\"PSD / dB wrt m$^{2}$ s$^{-4}$ Hz$^{-1}$\")\n",
    "p=convert_to_acc_dB(nlnmf, nlnmvelpsd)\n",
    "plt.semilogx(1./nlnmf, p, label='NLNM', linewidth=5, color='grey')\n",
    "for case in (foutra_stdPSD, foutra_nosmoothPSD):\n",
    "    for label in case:\n",
    "        if label != \"frequency\":\n",
    "            p=convert_to_acc_dB(case[\"frequency\"][1:], case[label][1:])\n",
    "            plt.semilogx(1./case[\"frequency\"][1:], p, ls='--', label=label)\n",
    "p=convert_to_acc_dB(welchPSD[0], welchPSD[1])\n",
    "plt.semilogx(1./welchPSD[0], p, ls=':', label=\"scipy.signal.welch\")\n",
    "p=convert_to_acc_dB(ppsd_f, ppsd_psd)\n",
    "plt.semilogx(1./ppsd_f, p, label=\"obspy.signal.PPSD\")\n",
    "plt.semilogx(1./ff[1:], convert_to_acc_dB(ff[1:], percentile_50[1:]), \n",
    "             color = 'k', label = 'scipy.signal.spectrogram median')\n",
    "plt.grid(linewidth=0.5, linestyle='--')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
