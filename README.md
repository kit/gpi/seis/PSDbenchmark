# Power-spectral density benchmark
This project provides benchmark computations to confirm the consistency
of different implementations for the computation of power-spectral density
(PSD).

## Implementations
The following approaches to PSD computation are compared based on identical
test time series.

### Parceval's theorem
For white (uncorrelated) stationary stochastic time series PSD is
computed from the rms amplitude of the time series.
This value serves as a reference.

### `scipy.signal.welch`
Implementation of Welch's (1967) algorithm as provided in the scipy module
signal.
This function outputs PSD values with uniform sampling on a linear frequency
scale.
At any frequency the same amount of averaging is applied, such that the
stochastic scatter appears in a uniform corridor along frequency.

### `foutra`
Spectral analysis tool provided in 
[Seitosh](https://git.scc.kit.edu/Seitosh/Seitosh/-/blob/master/src/ts/wf/foutra.cc).
The program can be configured to average and output in uniform intervals on a
logarithmic frequency scale.
Hence the amount of values averaged increase with frequency, making the
scatter corridor to become narrower with increasing frequency.

### `croposp`
Spectral analysis and three-channel correlation analysis tool provided in
[Seitosh](https://git.scc.kit.edu/Seitosh/Seitosh/-/tree/master/src/ts/croposp).
The program can be configured to average and output in uniform intervals on a
logarithmic frequency scale.
Hence the amount of values averaged increase with frequency, making the
scatter corridor to become narrower with increasing frequency.

### `obspy.signal.PPSD`
Computation of probabilistic power spectral densities as provided by ObsPy.
This module comes as a black box with no clear specification of the underlying
function for PSD computation.

## Files and directories
### File `PSDquantiles.ipynb`
Juypter notebook demonstrating that quantile statistics may not replace
proper averaging in the computation of PSD spectra from FFT coefficients.

### File `PSDtestwhite.ipynb`
Juypter notebook containing the report on the comparison for random white
noise. 
For white noise PSD is well defined and can easily be computed from the
time domain signal.
The reference values are clear in that case.
If PSD algorithm do not reproduce these values, the computation simply is
wrong.

### File `PSDtestdata.ipynb`
Prepared for a benchmark to be run on seismic recordings.
With actual recordings the analyzed signal no longer is entirely stochastic
and stationary.
Signal levels may vary with time and the signal may contain transients and
harmonic signals for which the PSD-scaling of FFT coefficients is
inappropriate.
This notebook serves as a testplace to see how this can be handled to find
meaningful, comparable results.

### File `readcroposp.py`
Python module to read PSD tables in the format output by croposp.

### File `seitoshio.py`
Python module to read ascii time series data with Seitosh headers.

### Data files
The following data files provide benachmark time series and PSD values
computed by `foutra` and `croposp`.

#### Seismic recording
    recording.asc
#### PSD values computed by `foutra` for seismic recording
    recording_nosmooth.tab
    recording_std.tab
#### Time series of gaussian random white noise
    white1.asc
    white2.asc
    white3.asc
#### PSD values computed by `croposp` for white noise 
    whitecroposp.tab
#### PSD values computed by `foutra` for white noise 
    whitefoutra.001.tab
    whitefoutra.002.tab
    whitefoutra.003.tab

## Prerequisites
To run the Jupyter notebooks an installation of [ObsPy](http://obspy.org)
is required.

## References
P. Welch, "The use of the fast Fourier transform for the
estimation of power spectra: A method based on time averaging
over short, modified periodograms", IEEE Trans. Audio
Electroacoust. vol. 15, pp. 70-73, 1967.

