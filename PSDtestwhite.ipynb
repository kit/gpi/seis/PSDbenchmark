{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Check consistency of PSD computation using white noise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Intention of this notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We check the validity of power spectral density (PSD) levels computed by different implementations.\n",
    "The test data is white noise.\n",
    "The time series is computed by [randomseries](https://git.scc.kit.edu/Seitosh/Seitosh/-/blob/master/src/synt/misc/randomseries.cc).\n",
    "These data shall be analyzed by different approaches to demonstrate that they all provide the same values of PSD.\n",
    "Additionally the computations in the Fourier domain are compared with the levels computed in the time domain by means of Parceval's theorem (which is only possible for white noise).\n",
    "\n",
    "PSD values are computed in three ways:\n",
    "1. from the rms-amplitude in the time domain according to Parceval's theorem\n",
    "2. The results of the function `scipy.signal.welch` as used in CONRAD.\n",
    "3. by the program [croposp](https://git.scc.kit.edu/Seitosh/Seitosh/-/tree/master/src/ts/croposp)\n",
    "4. by the program [foutra](https://git.scc.kit.edu/Seitosh/Seitosh/-/blob/master/src/ts/wf/foutra.cc)\n",
    "5. PSD data extracted from an instance of `obspy.signal.PPSD`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Computations not carried out in python are controlled by the accompanying `Makefile`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### White noise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Three series of white noise are produced with the program [randomseries](https://git.scc.kit.edu/Seitosh/Seitosh/-/blob/master/src/synt/misc/randomseries.cc) and a high-quality random number generator from [libgsl](https://www.gnu.org/software/gsl/doc/html/rng.html).\n",
    "The series consist of gaussian distributed uncorrelated random numbers with a given variance and mean value.\n",
    "The time series are provided in [Seitosh ascii format](https://git.scc.kit.edu/Seitosh/Seitosh/-/tree/master/src/libs/libdatrwxx/ascii) in files\n",
    "- `white1.asc`\n",
    "- `white2.asc`\n",
    "- `white3.asc`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### PSD values computed with `croposp`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameters used for the computation with `croposp` are set in the `Makefile`.\n",
    "The results are provided in an ASCII table in file `whitecroposp.tab`.\n",
    "The first column specifies frequency for each line in units of Hertz.\n",
    "The next columns specify PSD in units of $\\text{counts}^2\\, \\text{Hz}^{-1}$ if the time series samples are given in units of $\\text{counts}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### PSD values computed with `foutra`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameters used for the computation with `foutra` are set in the `Makefile`.\n",
    "The results computed for the three time series are provided in an ASCII table in files `whitefoutra.001.tab`, `whitefoutra.002.tab`, and `whitefoutra.003.tab`.\n",
    "The first column specifies frequency for each line in units of Hertz.\n",
    "The second column specifies PSD in units of $\\text{counts}^2\\, \\text{Hz}^{-1}$ if the time series samples are given in units of $\\text{counts}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prepare for later computation\n",
    "### Import python modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.signal as signal\n",
    "from obspy.signal import PPSD\n",
    "from scipy.signal import spectrogram\n",
    "from obspy import Stream\n",
    "import matplotlib.mlab as mlab"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from readcroposp import readcroposp\n",
    "from seitoshio import readascii"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Do some elementary settings for diagram displays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams[\"figure.figsize\"] = (20,10)\n",
    "plt.rcParams[\"axes.titlesize\"] = 24\n",
    "plt.rcParams[\"axes.labelsize\"] = 20\n",
    "plt.rcParams[\"font.size\"] = 20\n",
    "plt.rcParams[\"xtick.labelsize\"] = 15\n",
    "plt.rcParams[\"ytick.labelsize\"] = 15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyze time series data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read time series data.\n",
    "The time series provided in files `white1.asc`, `white2.asc`, and `white2.asc` are composed of uncorrelated gaussian distributed random numbers with a constant offset.\n",
    "They are time series of stationary white random noise, because the random numbers are uncorrelated.\n",
    "The parameters in terms of signal power, number of samples, sampling interval, and offset are identical for all three of them.\n",
    "They are just three different representations of random sequences with these parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read the time series data. \n",
    "Because of the large number of samples, this may take a while.\n",
    "Let the functions output a lot of information, just for the impatient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "whiteseries=dict()\n",
    "for i in range(1,4):\n",
    "    filename='white%d.asc' % i\n",
    "    inputseries=readascii(filename, verbose=True, demean=False)\n",
    "    label=\"%s.%s.%s\" % (inputseries.stats.station, \n",
    "                        inputseries.stats.instype,\n",
    "                        inputseries.stats.channel)\n",
    "    inputseries.stats.update({\"filename\": filename,\n",
    "                             \"label\": label})\n",
    "    whiteseries[label]=inputseries\n",
    "    print('\\n', inputseries.stats, '\\n')\n",
    "    # store Nyquist frequency as the upper limit of the frequency scale\n",
    "    fNy=0.5/inputseries.stats.delta"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute PSD level by using Parceval's theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Parceval's theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If $a(t)$ is the time domain signal and\n",
    "$$\n",
    "P(\\tau)=\\lim_{T\\rightarrow\\infty}\\frac{1}{2T}\\int\\limits_{-T}^{+T}a(t)\\,a(t+\\tau)\\,\\text{d}t\n",
    "$$\n",
    "is the normalized autocorrelation, then\n",
    "$$\n",
    "\\tilde{P}(\\omega)=\\int\\limits_{-\\infty}^{+\\infty}P(\\tau)\\,e^{-i\\omega \\tau}\\,\\text{d}\\tau\n",
    "$$\n",
    "is the *two-sided* power spectral density (PSD)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Average total signal power is\n",
    "$$\n",
    "\\overline{P}=\\lim_{T\\rightarrow\\infty}\\frac{1}{2T}\\int\\limits_{-T}^{+T}a(t)^2\\,\\text{d}t=P(\\tau=0).\n",
    "$$\n",
    "If we understand $\\tilde{P}(\\omega)$ as part of a Fourier transformation pair, then\n",
    "$$\n",
    "P(\\tau)=\\int\\limits_{-\\infty}^{+\\infty}\\tilde{P}(\\omega)\\,e^{i\\omega \\tau}\\,\\frac{\\text{d}\\omega}{2\\pi}.\n",
    "$$\n",
    "This way we can express the average total signal power by\n",
    "$$\n",
    "\\overline{P}=P(\\tau=0)=\\int\\limits_{-\\infty}^{+\\infty}\\tilde{P}(\\omega)\\,\\frac{\\text{d}\\omega}{2\\pi}.\n",
    "$$\n",
    "Hence\n",
    "$$\n",
    "\\int\\limits_{-\\infty}^{+\\infty}\\tilde{P}(\\omega)\\,\\frac{\\text{d}\\omega}{2\\pi}=\\lim_{T\\rightarrow\\infty}\\frac{1}{2T}\\int\\limits_{-T}^{+T}a(t)^2\\,\\text{d}t,\n",
    "$$\n",
    "which essentially is *Parceval's theorem*.\n",
    "From this it is obvious that stationary signals must be band-limited in order to have finite average total signal power.\n",
    "A not band-limited signal would have infinite power.\n",
    "\n",
    "If $P_\\text{PSD}$ is a frequency independent level of *one-sided* power spectral density (PSD), as is the case for white noise, such that\n",
    "$$\n",
    "\\tilde{P}(\\omega)=\\begin{cases}\\frac{P_\\text{PSD}}{2}\\text{ if $|\\omega|<2\\pi f_{\\text{Ny}}$ and}\\\\ 0\\text{ otherwise,}\\end{cases}\n",
    "$$\n",
    "where $f_{\\text{Ny}}$ would be the Nyquist-frequency for a discrete time series, then the integration can be replaced by\n",
    "$$\n",
    "\\tilde{P}(\\omega)\\,2 f_{\\text{Ny}}=P_\\text{PSD}\\,f_{\\text{Ny}}=a^2_{\\text{rms}},\n",
    "$$\n",
    "where \n",
    "$$\n",
    "a_{\\text{rms}}=\\sqrt{\\frac{1}{2T}\\int\\limits_{-T}^{+T}a(t)^2\\,\\text{d}t}\n",
    "$$\n",
    "is the rms-amplitude of the stationary time domain signal.\n",
    "In consequence\n",
    "$$\n",
    "P_\\text{PSD}=\\frac{a^2_{\\text{rms}}}{f_{\\text{Ny}}}.\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Numerical computation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The rms value of the time series with $N$ samples $x_l$ is\n",
    "$$\n",
    "a_{\\text{rms}}=\\sqrt{\\frac{1}{N}\\,\\sum\\limits_{l=1}^{N}\\,(x_l-\\overline{x})^2}\n",
    "$$\n",
    "with\n",
    "$$\n",
    "\\overline{x}=\\sum\\limits_{l=1}^{N}\\,x_l.\n",
    "$$\n",
    "The total average power is $P=a^2_{\\text{rms}}$, where for white noise the power is uniformly distributed over the frequency band from 0 Hz to $f_{\\text{Ny}}$.\n",
    "The one-sided PSD hence is\n",
    "$$\n",
    "P_{\\text{PSD}}=\\frac{a^2_{\\text{rms}}}{f_{\\text{Ny}}}.\n",
    "$$\n",
    "Compute this value for each of the time series read and store them in a dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "whitePSD=dict()\n",
    "for series in whiteseries:\n",
    "    print(\"analyse %s\" % series)\n",
    "    dt=whiteseries[series].stats.delta\n",
    "    nsamples=len(whiteseries[series].data)\n",
    "    average=np.mean(whiteseries[series].data)\n",
    "    print(\"  sampling interval %6.3fs, %d samples, average value: %f10.3\" % (dt, nsamples, average))\n",
    "    rms=np.sqrt(np.mean(np.power((whiteseries[series].data-average),2)))\n",
    "    print(\"  rms amplitude: %f10.3\" % rms)\n",
    "    whitePSD[series]=rms*rms*2*dt\n",
    "    print(\"  one-sided PSD=%12.4f counts^2 Hz^(-1)\" % whitePSD[series])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "### Compute PSD by scipy-implementation of Welch's algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nperseg=500\n",
    "noverlap=int(0.5*nperseg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run computation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "welchPSD=dict()\n",
    "for series in whiteseries.keys():\n",
    "    print(\"analyse %s\" % series)\n",
    "    dt=whiteseries[series].stats.delta\n",
    "    f, psd=signal.welch(whiteseries[series].data, 1./dt, nperseg=nperseg, noverlap=noverlap)\n",
    "    # discard value at 0 Hz and value at Nyquist frequency\n",
    "    welchPSD[series]=[f[1:-1], psd[1:-1]]    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute PSD by matplotlib-implementation of Welch's algorithm\n",
    "\n",
    "The method matplotlib.mlab.psd is used in ObsPy's PPSD calculation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "welchPSDmpltlib = dict()\n",
    "for series in whiteseries.keys():\n",
    "    sampling_rate = whiteseries[series].stats.sampling_rate\n",
    "    spec, freq = mlab.psd(whiteseries[series].data, nperseg, sampling_rate,detrend=\"mean\", noverlap=noverlap)\n",
    "    # discard value at 0 Hz and value at Nyquist frequency    \n",
    "    welchPSDmpltlib[series]=[(freq[1:-1]),(spec[1:-1])]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute by `scpiy.spectrogram`\n",
    "\n",
    "Calculate spectrogram"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Switch averaging on and of, to investigate the effect on the median value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "applyaveraging=False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spectrogramPSD=dict()\n",
    "for series in whiteseries.keys():\n",
    "    print(\"analyse %s\" % series)\n",
    "    dt=whiteseries[series].stats.delta\n",
    "    ff, tt, S = spectrogram(whiteseries[series].data, \n",
    "                            fs=1./dt, \n",
    "                            window='hann', \n",
    "                            scaling='density',\n",
    "                            return_onesided=True,\n",
    "                            mode='psd',\n",
    "                            nperseg=nperseg, \n",
    "                            noverlap=noverlap)\n",
    "    if applyaveraging:\n",
    "        (nf, nw)=S.shape\n",
    "        # number of windows to average\n",
    "        Navg=100\n",
    "        # number of groups of windows\n",
    "        Ng=int(nw/Navg)\n",
    "        # number of windows needed for reshaping\n",
    "        Nww=Navg*Ng\n",
    "        # select submatrix appropriate for reshaping\n",
    "        Ssub=S[:,:Nww]\n",
    "        # reshape array\n",
    "        Srs=np.reshape(Ssub,(nf,Navg,Ng))\n",
    "        # compute average\n",
    "        Savg=np.mean(Srs,axis=1)\n",
    "        print(Savg.shape)\n",
    "        S=Savg.copy()\n",
    "    spectrogramPSD[series]=[ff,tt,S]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build and plot PSD"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ShistPSD=dict()\n",
    "\n",
    "for series in spectrogramPSD.keys():\n",
    "    ff,tt,S=spectrogramPSD[series]\n",
    "    a, b      = np.shape(S)\n",
    "    S_tot     = np.empty((a, 0), dtype = 'float64')\n",
    "    S_tot     = np.append(S_tot, S, axis=1)\n",
    "    # bb defines the binning along the PSD axis\n",
    "    # the value range of bb must cover the entire value range of actual PSD value\n",
    "    # if the range of bb is too small, this will bias the results\n",
    "    bb        = np.logspace(0, 3, 200)\n",
    "        \n",
    "    Shist= np.empty((len(bb)-1, len(ff)))\n",
    "    for i in range(len(ff)):\n",
    "        Shist[:,i] = np.histogram(S_tot[i,:], bins=bb)[0]/len(tt)\n",
    "\n",
    "    bb        = np.delete(bb, -1)\n",
    "\n",
    "    Shist[Shist == 0] = np.nan\n",
    "    \n",
    "    ShistPSD[series]=Shist\n",
    "\n",
    "    plt.figure(figsize=(9,6))\n",
    "    cmap = plt.get_cmap('RdYlBu_r')\n",
    "    plt.pcolormesh(ff, bb, Shist, cmap=cmap)\n",
    "    plt.colorbar().set_label(label='Probability',size=14)\n",
    "    plt.xlabel('Frequency / Hz', fontsize=14)\n",
    "    plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "    plt.xscale('log')\n",
    "    plt.yscale('log')\n",
    "    plt.title(series)\n",
    "    plt.xlim([1e-2, 20])\n",
    "    #plt.ylim([1e-8, 1])\n",
    "    plt.clim(0, np.nanmax(Shist)/np.sqrt(np.e))\n",
    "    plt.grid()\n",
    "    plt.tick_params(labelsize=10)\n",
    "    plt.tight_layout()\n",
    "    plt.show()\n",
    "    plt.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get Percentiles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "percentilesPSD=dict()\n",
    "for series in spectrogramPSD.keys():\n",
    "    ff,tt,S=spectrogramPSD[series]\n",
    "    Shist=ShistPSD[series]\n",
    "    a = len(ff)\n",
    "        \n",
    "    percentile_10 = np.zeros(a)\n",
    "    percentile_50 = np.zeros(a)\n",
    "    percentile_90 = np.zeros(a)\n",
    "    \n",
    "    for i in range(a):\n",
    "        \n",
    "        somma  = np.nancumsum(Shist[:, i])\n",
    "        vp_10  = np.amax(somma)*0.1\n",
    "        vp_90  = np.amax(somma)*0.9\n",
    "        vp_50  = np.amax(somma)*0.5\n",
    "        \n",
    "        p_10   = np.where(somma <= vp_10)\n",
    "        p_90   = np.where(somma <= vp_90)\n",
    "        p_50   = np.where(somma <= vp_50)\n",
    "        \n",
    "        #print(vp_10, len(p_10[0]), len(p_50[0]), len(p_90[0]))\n",
    "        \n",
    "        if len(p_10[0]) != 0:\n",
    "            \n",
    "            percentile_10[i] = bb[p_10[0][len(p_10[0]) - 1]]\n",
    "            \n",
    "        else:\n",
    "            \n",
    "            percentile_10[i] = 0\n",
    "            \n",
    "        if len(p_50[0]) != 0:\n",
    "            \n",
    "            percentile_50[i] = bb[p_50[0][len(p_50[0]) - 1]]\n",
    "            \n",
    "        else:\n",
    "            \n",
    "            percentile_50[i] = 0\n",
    "        \n",
    "        if len(p_90[0]) != 0:\n",
    "            \n",
    "            percentile_90[i] = bb[p_90[0][len(p_90[0]) - 1]]\n",
    "            \n",
    "        else:\n",
    "                \n",
    "                percentile_90[i] = 0\n",
    "    \n",
    "    nppercentile_10, nppercentile_50, nppercentile_90=np.percentile(S, [10, 50, 90], axis=1)\n",
    "        \n",
    "    plt.figure(figsize=(9,6))\n",
    "    plt.plot(ff, (percentile_10), color = 'r', label = '10th Percentile')\n",
    "    plt.plot(ff, (percentile_50), color = 'k', label = '50th Percentile')\n",
    "    plt.plot(ff, (percentile_90), color = 'b', label = '90th Percentile')\n",
    "    plt.plot(ff, (nppercentile_10), color = 'r', linestyle=':', label = '10th Percentile (numpy)')\n",
    "    plt.plot(ff, (nppercentile_50), color = 'k', linestyle=':', label = '50th Percentile (numpy)')\n",
    "    plt.plot(ff, (nppercentile_90), color = 'b', linestyle=':', label = '90th Percentile (numpy)')\n",
    "    plt.xlabel('Frequency / Hz', fontsize=14)\n",
    "    plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "    plt.ylim([10., 200.])\n",
    "    plt.xlim([1e-2, 20])\n",
    "    plt.legend(fontsize=10)\n",
    "    plt.grid()\n",
    "    plt.xscale('log')\n",
    "    plt.yscale('log')\n",
    "    plt.title(series)\n",
    "    percentilesPSD[series]=[ff,percentile_10,percentile_50,percentile_90]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display results of PSD computation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read `croposp` result.\n",
    "In file `whitecroposp.tab` the values of PSD as returned by `croposp` are listed in columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "whitecropospPSD=readcroposp('whitecroposp.tab', verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read results of PSD analysis carried out with `foutra`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "whitefoutra=list()\n",
    "for i in range(1,4):\n",
    "    whitefoutra.append(readcroposp('whitefoutra.%3.3d.tab' % i, verbose=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create diagram and compare the values computed by `croposp` with the expected values based on the analysis of the white noise time series.\n",
    "This comparison makes sure that the computation of PSD is consistent with Parceval's theorem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minf=np.min(whitecropospPSD[\"frequency\"])\n",
    "maxf=fNy\n",
    "plt.figure()\n",
    "plt.title('Comparison of one-sided PSD computed by different approaches')\n",
    "plt.xlabel('frequency / Hz')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "for label in whitecropospPSD:\n",
    "    if label != \"frequency\":\n",
    "        plt.loglog(whitecropospPSD[\"frequency\"], whitecropospPSD[label], label=\"croposp: \"+label)\n",
    "\n",
    "for label in percentilesPSD.keys():\n",
    "    ff,percentile_10,percentile_50,percentile_90=percentilesPSD[series]\n",
    "    plt.loglog(ff, percentile_50, ls='-.', label=\"scipy.spectrogram median: \"+label)\n",
    "        \n",
    "for label in welchPSDmpltlib.keys():\n",
    "    [f, psd]=welchPSDmpltlib[label]\n",
    "    plt.loglog(f, psd, ls='-.', label=\"mpltlib: \"+label)\n",
    "    \n",
    "for label in welchPSD.keys():\n",
    "    [f, psd]=welchPSD[label]\n",
    "    plt.loglog(f, psd, ls=':',linewidth=3, label=\"scipy: \"+label)\n",
    "\n",
    "for case in whitefoutra:\n",
    "    for label in case:\n",
    "        if label != \"frequency\":\n",
    "            plt.loglog(case[\"frequency\"], case[label], ls='--', label=\"foutra: \"+label)\n",
    "for label in whitePSD:\n",
    "    plt.loglog([minf, maxf], [whitePSD[label], whitePSD[label]], label=\"time domain: \"+label, \n",
    "               linewidth=5, ls=':')\n",
    "plt.grid(linewidth=0.5, linestyle='--')\n",
    "plt.ylim(10.,200.)\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat comparison for foutra and croposp only, because scipy.signal welch outputs with uniform sampling on a linear frequency scale, while the other two output with uniform sampling on a logarithmic scale"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minf=np.min(whitecropospPSD[\"frequency\"])\n",
    "maxf=fNy\n",
    "plt.figure()\n",
    "plt.title('Comparison of one-sided PSD computed by different approaches')\n",
    "plt.xlabel('frequency / Hz')\n",
    "plt.ylabel(\"PSD / counts$^{2}$ Hz$^{-1}$\")\n",
    "for label in whitecropospPSD:\n",
    "    if label != \"frequency\":\n",
    "        plt.loglog(whitecropospPSD[\"frequency\"], whitecropospPSD[label], label=\"croposp: \"+label)\n",
    "for case in whitefoutra:\n",
    "    for label in case:\n",
    "        if label != \"frequency\":\n",
    "            plt.loglog(case[\"frequency\"], case[label], ls='--', label=\"foutra: \"+label)\n",
    "for label in whitePSD:\n",
    "    plt.loglog([minf, maxf], [whitePSD[label], whitePSD[label]], label=\"time domain: \"+label, \n",
    "               linewidth=5, ls=':')\n",
    "plt.grid(linewidth=0.5, linestyle='--')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Check consistency with `obspy.signal.PPSD`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`obspy.signal.PPSD` unfortunately is hardwired to expected data samples being in units of 1 m/s.\n",
    "There is no way to specify different units or to access the Axes object in order to adjust axes labels or axes limits.\n",
    "In the following we assume that data 1 count represents 1 nm/s."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute PSD by `obspy.signal.PPSD`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "paz = {'gain': 1., 'poles': [], 'sensitivity': 1., 'zeros': []}\n",
    "keys=list(whiteseries.keys())\n",
    "print(keys)\n",
    "stats=whiteseries[keys[0]].stats\n",
    "for series in whiteseries.keys():\n",
    "    tr=whiteseries[series].copy()\n",
    "    ppsd = PPSD(tr.stats, paz, ppsd_length=stats.delta*nperseg*10)\n",
    "    # convert to m/s\n",
    "    tr.data = tr.data *1.e-9\n",
    "    st=Stream(tr)\n",
    "    ppsd.add(st)\n",
    "#print(ppsd.times_processed) \n",
    "    ppsd.plot(show_noise_models=False) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ppsd_psd_list=ppsd.psd_values\n",
    "ppsd_period=ppsd.period_bin_centers\n",
    "ppsd_f=1./ppsd_period\n",
    "print(ppsd_f.shape)\n",
    "print(len(ppsd_psd_list))\n",
    "ppsd_psd=np.zeros(ppsd_f.shape)\n",
    "for p in ppsd_psd_list:\n",
    "# values are given in dB wrt to 1. m**2/(s**4 Hz)\n",
    "# convert to units of 1. nm**2/(s**2 Hz)\n",
    "    p = 10.**(p/10.)*1.e18/(2.*np.pi*ppsd_f)**2\n",
    "    ppsd_psd = ppsd_psd + p\n",
    "ppsd_psd = ppsd_psd / len(ppsd_psd_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create diagram on dB scale"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### convert data in units of 1 nm^2/(s^2 Hz) to 1 dB wrt 1 m^2/(s^4 Hz)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def convert_to_acc_dB(f, psd):\n",
    "    p=10*np.log10(psd * 1.e-18 * (2.* np.pi * f)**2)\n",
    "    return p"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minT=1./fNy\n",
    "maxT=np.max(1./whitecropospPSD[\"frequency\"])\n",
    "plt.figure()\n",
    "plt.title('Comparison of one-sided PSD computed by different approaches')\n",
    "plt.xlabel('period / s')\n",
    "plt.ylabel(\"PSD / dB wrt 1 m$^{2}$ s$^{-4}$ Hz$^{-1}$\")\n",
    "plt.xlim(minT,maxT)\n",
    "for label in whitecropospPSD:\n",
    "    if label != \"frequency\":\n",
    "        p=convert_to_acc_dB(whitecropospPSD[\"frequency\"], whitecropospPSD[label])\n",
    "        plt.semilogx(1./whitecropospPSD[\"frequency\"], p, label=\"croposp: \"+label)\n",
    "for label in welchPSD.keys():\n",
    "    [f, psd]=welchPSD[label]\n",
    "    p=convert_to_acc_dB(f, psd)\n",
    "    plt.semilogx(1./f, p, ls=':', label=\"scipy: \"+label)\n",
    "for label in welchPSDmpltlib.keys():\n",
    "    [f, psd]=welchPSDmpltlib[label]\n",
    "    p=convert_to_acc_dB(f, psd)\n",
    "    plt.semilogx(1./f, p, ls='-.', label=\"mpltlib: \"+label)    \n",
    "for case in whitefoutra:\n",
    "    for label in case:\n",
    "        if label != \"frequency\":\n",
    "            p=convert_to_acc_dB(case[\"frequency\"], case[label])\n",
    "            plt.semilogx(1./case[\"frequency\"], p, ls='--', label=\"foutra: \"+label)\n",
    "p=convert_to_acc_dB(ppsd_f, ppsd_psd)\n",
    "plt.semilogx(1./ppsd_f, p, label=\"obspy.signal.PPSD\")\n",
    "for label in whitePSD:\n",
    "    f=np.array([minf, maxf])\n",
    "    p=convert_to_acc_dB(f, np.array([whitePSD[label], whitePSD[label]]))\n",
    "    plt.semilogx(1./f, p, label=\"time domain: \"+label, \n",
    "               linewidth=5, ls=':')\n",
    "plt.grid(linewidth=0.5, linestyle='--')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
